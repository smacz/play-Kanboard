# Kanboard Play

Installs Kanboard web service with `mariadb` backend.

## Invocation

```shell
# git clone <environment repo> ./environment
# ansible-galaxy install -r requirements.yml
# ansible-playbook playbooks/site.yml
```
